let pie_col_gen = d3.scaleOrdinal(d3.schemePastel1);
let pie_colors = []

for (i = 0; i < 10; i++) {
    pie_colors.push(pie_col_gen(i))
}

let getColorGroup = (function (rgb, num) {
    let colors = []
    rgb = d3.rgb(rgb).darker(0.1).formatHex().toString()
    for (i = 0; i < num; i++) {
        rgb = d3.rgb(rgb).darker(0.1).formatHex().toString()
        colors.push(rgb)
    }
    return colors
})

// Set new default font family and font color to mimic Bootstrap's default styling
pie_chart_data = {
    labels: ["None"],
    datasets: []
}

// Pie Chart Example
var ctx = document.getElementById("myPieChart");
var myPieChart = new Chart(ctx, {
    type: 'doughnut',
    data: pie_chart_data,
    options: {
        maintainAspectRatio: false,
        tooltips: {
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#858796",
            borderColor: '#dddfeb',
            borderWidth: 1,
            xPadding: 20,
            yPadding: 20,
            displayColors: true,
            caretPadding: 20,
            callbacks: {
                label: function (tooltipItem, data) {
                    let label = data.labels[tooltipItem.index];
                    let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]
                    label = `${label}: ${value} (${ec_desc[label]})`
                    return label;
                }
            },
        },
        legend: {
            display: true,
            position: 'bottom',
            labels: {
                padding: 10,
                boxWidth: 20,
                cutoutPercentage: 50,
                filter: function(legend){
                    if (legend.text.indexOf(".") < 0) {
                        return true
                    }
                },
            }
        },
    }
});

function anno_pie_chart(ec_cate) {
    myPieChart.data.labels = []
    myPieChart.data.datasets = []
    const add = (a, b) => a + b

    // outer layer
    let labels = []
    let ecsub_obj = { data: [], backgroundColor: [] }

    let cate_size = Object.keys(ec_cate).length

    // Adding inner colors
    ecsub_obj.backgroundColor = ecsub_obj.backgroundColor.concat(pie_colors.slice(0, Object.keys(ec_cate).length))

    // padding outer layer with zeros
    let zeros = new Array(cate_size).fill(0);
    ecsub_obj.data = ecsub_obj.data.concat(zeros)

    let base_colors = pie_colors.slice(0, ecsub_obj.data.length)

    $.each(ec_cate, function (ec1, ec_sub) {
        // append labels for the first layer EC cates
        labels = labels.concat(Object.keys(ec_sub))
        ecsub_obj.data = ecsub_obj.data.concat(Object.values(ec_sub))
        bc = base_colors.shift()
        ecsub_obj.backgroundColor = ecsub_obj.backgroundColor.concat(getColorGroup(bc, Object.keys(ec_sub).length))
    })

    myPieChart.data.datasets.push(ecsub_obj)

    // inner layer labels
    myPieChart.data.labels = myPieChart.data.labels.concat(Object.keys(ec_cate))

    // inner layer
    ecsub_obj = { data: [], backgroundColor: [] }

    $.each(ec_cate, function (ec1, ec_sub) {
        ecsub_obj.data.push(Object.values(ec_sub).reduce(add))
    })
    myPieChart.data.datasets.push(ecsub_obj)

    // outer labels
    myPieChart.data.labels = myPieChart.data.labels.concat(labels)
    // Adding inner colors
    ecsub_obj.backgroundColor = ecsub_obj.backgroundColor.concat(pie_colors.slice(0, Object.keys(ec_cate).length))

    myPieChart.update()
}
