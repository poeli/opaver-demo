let mapImg
let kgml
let interval
let prev_mapid = []
let pie_chart_data = {}
let display_exp_val_pref = {}
let display_badge = true
let display_gene_name = false
let exp_color

let is_global_overview_map = (mapid) => {
	return (mapid.startsWith("011") || mapid.startsWith("012"))
}

let _ctrl_params = () => {
	id = $('#home-pathway-id').text()
	t = $('#timepoint-selector > div.active').attr("data-value")
	hm = $('#conflicts-handler > div.active').attr("data-value")
	return [id, Number(t), hm]
}

let alertError = (msg) => {
	$(".error-message > span").html(msg);
	$(".error-message").show();
	setTimeout(function(){
		$(".error-message").fadeOut(); 
	}, 10000);
}

let loading = $('#loading-spinner').hide()
$(document).ajaxStart( () => {
    loading.fadeIn();
  }).ajaxStop( () => {
    loading.fadeOut();
  });

//
// addController: init controllers
// 

function addController(mapdiv, mapid, proj_dir, tc, color, scaleMin, scaleMax) {
	// Zoom buttons
	$('#map-zoom-out').on("click", function () {
		let cur_zoom = Number($('#keggmap').css('zoom')) || 1;
		$('#keggmap').css('zoom', cur_zoom - 0.1)
	})
	$('#map-zoom-reset').on("click", function () {
		$('#keggmap').css('zoom', 1)
	})
	$('#map-zoom-in').on("click", function () {
		let cur_zoom = Number($('#keggmap').css('zoom')) || 1;
		$('#keggmap').css('zoom', cur_zoom + 0.1)
	})

	// Autoplay controller 
	$('#autoplay-switch > div').on("click", function () {
		if ($(this).attr("data-value") != "off") {
			let ivl_sec = Number($(this).attr("data-value"))

			// clear existing interval
			window.clearInterval(interval);

			$('#autoplay-switch > div').removeClass("active")
			$(this).addClass("active")

			interval = window.setInterval(function () {
				let target_tc = $('#timepoint-selector > div.active').next()
				if (target_tc.length == 0) {
					target_tc = $('#timepoint-selector > div:first')
				}
				target_tc.click()
			}, ivl_sec * 1000);
		}
		else {
			window.clearInterval(interval);
			$(this).parent().find("div").removeClass('active');
			$(this).addClass('active');
		}
	});

	// Conflicts-handler
	$('#conflicts-handler > div').on("click", function () {
		let [id, t, hm] = _ctrl_params()
		hm = $(this).attr("data-value")
		$(this).parent().find("div").removeClass('active');
		$(this).addClass('active');
		drawMap(mapdiv, id, display_badge, proj_dir, t, hm);
	});

	// Badge-handler
	$('#badge-handler > div').on("click", function () {
		let [id, t, hm] = _ctrl_params()
		temp = $(this).attr("data-value")
		if( temp=="y"){
			display_badge = true
			$('.gene-number-icon').show()
		}
		else{
			display_badge = false
			$('.gene-number-icon').hide()
		}
		$(this).parent().find("div").removeClass('active');
		$(this).addClass('active');
		//drawMap(mapdiv, id, display_badge, proj_dir, t, hm);
	});

	// Display gene-name-handler
	$('#name-handler > div').on("click", function () {
		temp = $(this).attr("data-value")
		if( temp=="id"){
			display_gene_name = false
			$('.enzyme-area-number-tag').each(function(){
				let text = $(this).attr('data-id')
				$(this).first().html(text)
			})
		}
		else{
			display_gene_name = true
			$('.enzyme-area-number-tag').each(function(){
				let text = $(this).attr('data-gene')
				$(this).first().html(text)
			})
		}
		$(this).parent().find("div").removeClass('active');
		$(this).addClass('active');
	});

	// timepoint changer
	$('#tc-previous').on("click", function () {
		$('#timepoint-selector > div.active').prev().click()
	})
	$('#tc-next').on("click", function () {
		$('#timepoint-selector > div.active').next().click()
	})

	// loading settings from settings.json file and adding time course options
	let settings_json = `${proj_dir}/settings.json`;

	let loadSettings = $.getJSON(settings_json, function (data) {
		// adding timepoint down-select menu
		let selectdom = $('#timepoint-selector');
		$.each(data, function (idx, tpinfo) {
			let odom = $(`<div id="tp_selec${idx}" data-value="${idx}" class="collapse-item">${tpinfo.desc}</option>`);
			if (tc == idx) {
				odom.addClass("active");
			}
			odom.on("click", function () {
				$(this).parent().find("div").removeClass('active');
				$(this).addClass('active');
				let [id, t, hm] = _ctrl_params()
				drawMap(mapdiv, id, display_badge, proj_dir, idx, hm);
			});
			odom.appendTo(selectdom);
		});

		// store settings_data
		localStorage.setItem('settings', JSON.stringify(data))
	})

	$.when(loadSettings).done(function () {
		settings_data = loadSettings.responseJSON

		// retrieve the list of all KEGG map
		let pwy_url = `${proj_dir}/overall_pathway.json`;
		let loadPwy = $.getJSON(pwy_url, function (data) {
			// add pathway map down-select menu
			data = data.sort(function (a, b) {
				return Number(b.sum_act) - Number(a.sum_act);
			});

			let selectdom = $('#pathway-selector');
			$.each(data, function (idx, pwy) {
				idx = pwy.mapid;
				let note = "";
				// add non-overview map only
				if (is_global_overview_map(idx) == false) {
					//note = " [View only]";
					let select_flag = "";
					if (!mapid) { mapid = idx }
					if (mapid == idx) {
						select_flag = "selected";
					}
					let odom = $(`<option id="map_selec${idx}" value="${idx}" ${select_flag}>${idx} ${pwy.name} (${pwy.tc_act})</option>`);
					selectdom.append(odom);
				}
			});
		});

		$.when(loadPwy).done(function () {
			$('#pathway-selector').selectpicker('refresh');

			$('#pathway-selector').on("change", function () {
				// when mapid selected, re-draw the kegg map
				mapid = $('#pathway-selector').val()
				let [id, t, hm] = _ctrl_params()
				drawMap(mapdiv, mapid, display_badge, proj_dir, t, hm);
			});
		})
	})

	// color scale event
	$('#scalebar-min').val(Number(scaleMin))
	$('#scalebar-max').val(Number(scaleMax))

	$('#scalebar-min').on("change", function () {
		try {
			val = Number(this.value)
			if (val < scaleMax) {
				scaleMin = val
				exp_color = new expColor(scaleMin, scaleMax, color);
				mapid = $('#pathway-selector').val()
				let [id, t, hm] = _ctrl_params()
				drawMap(mapdiv, mapid, display_badge, proj_dir, t, hm);
			}
			else {
				alert("Min value must be less than max value.")
			}
		}
		catch (error) {
			console.error(error);
		}
	})

	$('#scalebar-max').on("change", function () {
		try {
			val = Number(this.value)
			if (val > scaleMin) {
				scaleMax = val
				exp_color = new expColor(scaleMin, scaleMax, color);
				mapid = $('#pathway-selector').val()
				let [id, t, hm] = _ctrl_params()
				drawMap(mapdiv, mapid, display_badge, proj_dir, t, hm);
			}
			else {
				alert("Max value must be greater than min value.")
			}
		}
		catch (error) {
			console.error(error);
		}
	})

	// init colors
	exp_color = new expColor(scaleMin, scaleMax, color);
}

//
// drawMap
// 

function drawMap(mapdiv, mapid, display_badge, proj_dir, tc, conflict_handler) {
	settings_data = JSON.parse(localStorage.getItem('settings'))
	mapid = mapid;        //initial map

	let ec_types = settings_data[tc].ec_type
	let cpd_types = settings_data[tc].cpd_type
	let tc_dir = settings_data[tc].dir
	let expjson = `${proj_dir}/${tc_dir}/map${mapid}.exp.json`;
	let loadKGML;
	let up_reg_num = 0
	let dn_reg_num = 0
	let re_render_all_entry_block = true

	org = settings_data[tc].org_type //default organism id type: ec or ko
	let mapxml = `kegg_info/kegg_pathway_kgml/${org}${mapid}.xml`;

	// updating cards/indicators
	$('#home-pathway-id').html(mapid)
	$('#home-timepoint-desc').html(settings_data[tc].desc)
	//$('#keggmap-ts').html(settings_data[tc].desc)

	// Init pathway map
	if ($("#map-image").length) {
		mapImg = $("#map-image")
	}
	else {
		mapImg = $("<img id='map-image'/>");
		$(`#${mapdiv}`).append(mapImg)
	}

	// sync selectpicker to the same map
	$('#pathway-selector').selectpicker('val', mapid);

	// when map changes
	if (mapImg.attr("alt") != mapid) {
		$(`#${mapdiv} a`).remove()
		//loading background pathway map		
		mapImg.attr("src", `kegg_info/kegg_pathway_image/map${mapid}.png`);
		mapImg.attr("alt", mapid);
		mapImg.on("load", function () {
			let naturalWidth = $('#map-image')[0].naturalWidth;
			let naturalHeight = $('#map-image')[0].naturalHeight;
			//$(`#${mapdiv}`).width(naturalWidth);
			$(`#${mapdiv}`).height(naturalHeight);
			$('#map-image').width(naturalWidth);
			$('#map-image').height(naturalHeight);
		});

		// load KGML
		loadKGML = $.ajax({
			type: "get",
			url: mapxml,
			dataType: "xml",
			success: function (data) {
				kgml = data
			},
			error: function(xhr, status, text) {
				// var response = $.parseJSON(xhr.responseText);
				if (text) {
					alertError(text)
				} else {
					alertError(`ERR_CONNECTION_REFUSED: This site can’t be reached.`)
				}
				loading.fadeOut()
				$('#map-image').remove()
			}
		});

		// update type/method indicators
		if( ec_types.length > 0 ){
			$('#entry-ec-type-identifier').parent().show()
			$(`#entry-ec-type-identifier > button`).remove()

			$(ec_types).each(function(idx, val){
				let holder = $('#entry-ec-type-identifier')
				let type_tag = $(`<button type="button" class="btn btn-sm btn-primary" disabled>`)
				type_tag.html(val)
				type_tag.appendTo(holder)
			})
		}
		else {
			$('#entry-ec-type-identifier').parent().hide()
		}

		if( cpd_types.length > 0 ){
			$('#entry-cpd-type-identifier').parent().show()
			$(`#entry-cpd-type-identifier > button`).remove()

			$(cpd_types).each(function(idx, val){
				let holder = $('#entry-cpd-type-identifier')
				let type_tag = $(`<button type="button" class="btn btn-sm btn-primary" disabled>`)
				type_tag.html(val)
				type_tag.appendTo(holder)
			})	
		}
		else {
			$('#entry-cpd-type-identifier').parent().hide()
		}


		// update pie chart
		anno_pie_chart(gene_map_analysis_data[mapid]['ec_stats']);
	}

	//load expression/annotation json file
	$.ajax({
		type: "get",
		url: expjson,
		dataType: "json",
		success: function (annojson) {
			// Find all graphics object in KGML:
			//
			// Example XML entry:
			// <entry id="66" name="ec:2.7.1.2" type="enzyme" reaction="rn:R01600"
			// 	link="http://www.kegg.jp/dbget-bin/www_bget?2.7.1.2">
			// 	<graphics name="2.7.1.2" fgcolor="#000000" bgcolor="#BFBFFF"
			// 		 type="rectangle" x="233" y="312" width="46" height="17"/>
			// </entry>
			$.when(loadKGML).done(function () {
				$(kgml).find("graphics").each(function () {
					let entry_type = $(this).parent().attr("type");
					let graphic_type = $(this).attr("type");

					// SKIPPING any lines
					if (graphic_type == 'line') { return true; }
					// Handling ENZYME/ORTHOLOG/COMPOUND
					if (entry_type == "enzyme" || entry_type == "ortholog" || entry_type == "compound") {
						let url = $(this).parent().attr("link");
						let name = $(this).parent().attr("name");
						let cid = $(this).attr("name");
						let w = Number($(this).attr("width")) + 2
						let h = Number($(this).attr("height")) + 2
						let x = $(this).attr("x") - 1 - w / 2;
						let y = $(this).attr("y") - 2 - h / 2;
						let w_block = w / ec_types.length
						let types = (entry_type == "compound" ? cpd_types: ec_types)

						// Retrieve all ids of entries (ec or ko) having expression data
						let idstring = name.replace(/\w+:/g, "");
						let idarray = idstring.split(" ");
						let ids = $.map(idarray, (eid) => {
							if (typeof (annojson[eid]) != 'undefined') {
								return eid;
							}
						});

						// For the blocks having multiple EC/KOs, rename the block to '{id}..'
						let entry_name = ids[0]
						if (ids.length>1){
							entry_name = `${ids[0]}..`
						}
						let entry_gene = entry_name

						// Each enzyme has a wrapper-block that holds available expression block
						// and provide tooltips
						let wrapper_dom;
						if (entry_type == "compound"){
							wrapper_dom = $(`<a class='compound-area entry-block' data-timepoint='${mapid}-${tc}' href='${url}' target='new' title='${name}' alt='${name}' style='left: ${x-2}px; top: ${y-2}px;'></a>`);
						}
						else{
							wrapper_dom = $(`<a class='enzyme-area entry-block' data-timepoint='${mapid}-${tc}' data-map='${mapid}' alt='${name}' href='${url}' target='new' title='${name}'></a>`);
							wrapper_dom.css("left", x + "px");
							wrapper_dom.css("top", y + "px");
							wrapper_dom.css("width", w + "px");
							wrapper_dom.css("height", h + "px");
						}
						wrapper_dom.tooltipster({ interactive: true });	

						// type counts
						let type_vals = {}
						let type_doms_list = []
						let icon_doms_list = []

						// adding KEGG enzyme information to the tooltip content
						let tooltipcontent = $('<div class="tooltipcontent">');

						// add unannotated COMPOUND image to tooltip content
						if ( ids.length==0 ){
							if (entry_type == "compound") {
								let img = `kegg_info/kegg_compound_image/${cid}.png`;
								tooltipcontent
									.append($(`<span class="tag-label">${name}</span>`))
									.append($(`<img src="${img}"/>`));
							}
						}

						// Iterates enzymes that have expression data.
						$.each(ids, function (index, id) {
							// Iterates all expression types for each enzyme
							// Generate an individual block for each expression type.
							// 1) create a tooltip content
							// 2) creating a div and filling a color reflecting the expression level
							// 3) add the number icon

							let idtag = $(`<span class="tag-label entry-name">${id}</span>`);
							// idtag.on("click", function () {
							// 	let hl = new highlightBlock('#'+mapdiv)
							// 	hl.enable(id)
							// });

							if(annojson[id].info.name.length > 6){
								entry_gene = annojson[id].info.name.substring(0, 5);
								if(entry_gene.endsWith(',') || entry_gene.endsWith(', ')){
									entry_gene = entry_gene.substring(0, 4)
								}
								entry_gene += ".."
							}
							else{
								entry_gene = annojson[id].info.name
							}

							tooltipcontent.append(idtag);
							let tabledom = $('<table class="gene-info ui-responsive">');
							let tbodydom = $('<tbody>');
							$('<tr>')
								.append($('<td>').html('Name'))
								.append($('<td>').html(annojson[id].info.name))
								.appendTo(tbodydom);

							let pathwayTd = $('<td>');
							let pwys = annojson[id].info.pathway.split(", ");
							$.each(pwys, function (idx, pwy) {
								if (is_global_overview_map(pwy)==false){
									$('<a data-ajax="false" style="margin-right:0.5em; cursor: pointer;">')
									.html(pwy)
									.on("click", function () { if (pwy != mapid) { drawMap(mapdiv, pwy, display_badge, proj_dir, tc, conflict_handler) } })
									.appendTo(pathwayTd);
								}
							});

							$('<tr>')
								.append($('<td>').html('Pathway'))
								.append(pathwayTd)
								.appendTo(tbodydom);
							$('<tr>')
								.append($('<td>').html('Desc'))
								.append($('<td>').html(annojson[id].info.definition))
								.appendTo(tbodydom);

							if (entry_type == "compound"){
								let img = `kegg_info/kegg_compound_image/${cid}.png`;
								$('<tr>')
								.append($('<td>').html('Formula'))
								.append($('<td>').html(`<img src="${img}"></img>`))
								.appendTo(tbodydom);
							}

							//tabledom.table();
							tabledom.append(tbodydom);
							tooltipcontent.append(tabledom);

							// Integrates all expression types, adding following objects:
							// 1) a block filling with color
							// 2) counter icon
							// 3) table for all expression data
							$.each(types, function (index, type) {
								let blockid = entry_name.replace(/\./g, '-') + "-block-" + index;
								// Init a sub-block of expression type for the first time seeing this type.
								// Setting sub-block to absolute coordinate of the wrapper.
								// append the sub-block to a list
								if (typeof(type_vals[type])=="undefined"){
									// Init the dict using type as the key
									type_vals[type] = []

									let ex = w_block * index;
									let ey = 1;
									let block;
									
									if (entry_type == "compound"){
										// For the compound entry, opaver supports up to 2 types.
										if (index < 2){
											// The first type presents in left-half circle and the second presents in the right-half.
											w_block = 7
											block = $(`<div class="cpd-type-${index}">`);
											block.attr("data-type", type)
											block.css("left", (w_block*index) + "px");
											block.css("top", ey + 1 + "px");
											block.addClass(blockid)
											// If there is only one type, replace to the whole circle
											if(types.length==1){
												block.removeClass(`cpd-type-${index}`)
												block.addClass(`cpd-type-whole`)
											}	
										}
									}
									else{
										block = $('<div class="type-block">')
										block.attr("data-type", type)
										block.css("left", ex + "px");
										block.css("top", ey + "px");
										block.css("width", w_block + 1 + "px");
										block.css("height", (h + 1) + "px");
										block.addClass(blockid)

										// add counter badge(a circle icon)
										if (typeof (annojson[id][type]) != "undefined") {
											let ix = ex + w_block - 6;
											// for the first type, put the counter icon the left top corner.
											// Otherwise, put the counter icon to the right top corner.
											if (index == 0) { ix = -8 }
											let iy = -8;
											counterIcon = $(`<span class="badge badge-primary gene-number-icon">`)
											counterIcon.css("left", ix + "px");
											counterIcon.css("top", iy + "px");
											counterIcon.attr("data-type", type)
											icon_doms_list.push(counterIcon)
										}
									}
									type_doms_list.push(block)
								}

								// add expression data
								if ($(annojson[id][type]).length) {
									tooltipcontent.append($('<span>').html(`${type} (${Object.keys(annojson[id][type]).length})`));

									let tabledom = $('<table data-role="table" data-mode="reflow" class="gene-info ui-responsive">');
									tabledom.attr("data-type", type)
									let theaddom = $('<thead>');
									$('<tr>').append($('<th>').html('Entry'))
										.append( $('<th>').html('ID') )
										.append($('<th>').html('Differential Expression'))
										.appendTo(theaddom);

									// check if a saved preferred value is available for this type
									let saved_pref_val;
									try {
										saved_pref_val = display_exp_val_pref[mapid][tc][id][type];
									}
									catch (e) {
									}

									let tbodydom = $('<tbody>');
									
									$.each(annojson[id][type], function (input_ent, ent_info) {
										let diff = Number(ent_info.diff)
										type_vals[type].push(diff)

										let url_input_ent = `<a href="https://www.ncbi.nlm.nih.gov/gene/?term=${input_ent}" target="_new">${input_ent}</a>`;
										let exp_val_dom;

										if (saved_pref_val == diff) {
											exp_val_dom = $(`<span>${saved_pref_val}</span><i class="fas fa-check-circle ml-2"></i>`);
										}
										else {
											exp_val_dom = $(`<span style="cursor: pointer;">${diff}</span>`)
										}

										// set the clock of sub-block by clicking the value
										exp_val_dom.on("click", function () {
											// store pref value
											if (display_exp_val_pref[mapid] == undefined) {
												display_exp_val_pref[mapid] = {}
											}
											if (display_exp_val_pref[mapid][tc] == undefined) {
												display_exp_val_pref[mapid][tc] = {}
											}
											if (display_exp_val_pref[mapid][tc][entry_name] == undefined) {
												display_exp_val_pref[mapid][tc][entry_name] = {}
											}

											display_exp_val_pref[mapid][tc][entry_name][type] = diff

											// update sub-block to clicked value
											$('.'+blockid).css("background-color", exp_color.get_rgb(diff));

											$(this).closest('.tooltipcontent').find(`table[data-type="${type}"]`).find(".fa-check-circle").remove()
											$(this).append($('<i class="fas fa-check-circle ml-2"></i>'))
										})

										// adding Gene records to the tooltip tables
										let row = $('<tr>')
											.append($('<td>').html(url_input_ent))
											.append($('<td>').html(ent_info.orig_id))
											.append($(`<td>`).append(exp_val_dom))
											.appendTo(tbodydom);

										// counting up- or down-regulation genes
										if (diff > 0) { up_reg_num++; }
										else if (diff < 0) { dn_reg_num++; }
									});

									//tabledom.table();
									tabledom.append(theaddom);
									tabledom.append(tbodydom);
									tooltipcontent.append(tabledom);
								}
							})
						});

						// Finalize sub-block and appendTo block
						$.each(type_doms_list, (idx, block) => {
							let type = block.attr("data-type")

							if (type_vals[type].length>0) {
								// check if a saved preferred value is available for this type
								let saved_pref_val
								let preferred_val
								let cft_obj = new conflictHandler(conflict_handler)
								try {
									saved_pref_val = display_exp_val_pref[mapid][tc][entry_name][type];
								}
								catch (e) {
								}
	
								if (saved_pref_val != undefined) {
									preferred_val = saved_pref_val
								}
								else {
									$.each(type_vals[type], (idx, diff) => {
										cft_obj.add_value(diff)
									})
									preferred_val = cft_obj.resolve()
								}
	
								block.css("background-color", exp_color.get_rgb(preferred_val));	
							}
							wrapper_dom.append(block)
						})

						// Finalize GENE BADGES and appendTo sub-block
						$.each(icon_doms_list, (idx, counterIcon) => {
							let type = counterIcon.attr("data-type")
							if (type_vals[type].length > 1){
								// determining if there is a conflict of up- and down-regulation
								if (type_vals[type].some(v => v < 0) & type_vals[type].some(v => v > 0)){
									counterIcon.removeClass("badge-primary");
									counterIcon.addClass("badge-warning");						
								}
								counterIcon.html(type_vals[type].length)
								if (display_badge==false){ counterIcon.hide() }
								counterIcon.appendTo(wrapper_dom)
							}
							else{
								counterIcon.remove()
							}
						})

						// Add EC/KO name tag
						if (ids.length && is_global_overview_map(mapid) == false && entry_type != "compound") {
							let text = entry_name
							if(display_gene_name){
								text = entry_gene
							}
							let ec_tag_dom = $(`<div><div>${text}</div></div>`)
							ec_tag_dom.addClass("enzyme-area-number-tag");
							ec_tag_dom.css("top", "1px");
							ec_tag_dom.css("width", w + 1 + "px");
							ec_tag_dom.css("height", h + 1 + "px");
							ec_tag_dom.attr("data-id", entry_name);
							ec_tag_dom.attr("data-gene", entry_gene);
							wrapper_dom.append(ec_tag_dom)
						}

						// Activate tooltipsters if the tooltipcontent isn't empty
						wrapper_dom.tooltipster('destroy');
						if ( tooltipcontent.html() ) {
							wrapper_dom.tooltipster({
								content: tooltipcontent, 
								arrow: false, 
								interactive: true, 
								offsetY: -100, 
								position: 'right'
							});	
						}

						// updating cards
						$('#home-dn-reg-num').html(dn_reg_num)
						$('#home-up-reg-num').html(up_reg_num)

						// append entry block to the map
						wrapper_dom.appendTo($(`#${mapdiv}`));
					}
					//dealing with maps graphics //skip the title block of current map
					//else if (entry_type == "map" && $(this).attr("name")!="undefined" && !$(this).attr("name").startsWith("TITLE")) {
						else if (entry_type == "map") {
						let name = $(this).attr("name");
						let url = $(this).parent().attr("link");
						let entry_name = $(this).parent().attr("name");
						let mapid = entry_name.replace("path:" + org, "");
						let width = $(this).attr("width")
						let height = Number($(this).attr("height")) + 2
						let x = $(this).attr("x") - width / 2;
						let y = $(this).attr("y") - height / 2;

						let jdom = $("<a class='map-area' title='" + name + "' style='left:" + x + "px;top:" + y + "px;width:" + width + "px;height:" + height + "px;'></a>");
						let tooltipcontent = $('<div>');

						if ($(`#pathway-selector option[value="${mapid}"]`).length) {
							jdom.on("click", function () { $('#pathway-selector').val(mapid).change() })
							tooltipcontent
								.append($(`<span class="tag-label">${mapid}: ${name}</span>`))
								.append($(`<img src="kegg_info/kegg_pathway_image/map${mapid}.png" style="width: 500px; height: auto"></img>`));
						}
						else {
							tooltipcontent
								.append($('<span class="tag-label">' + mapid + ": " + name + '</span>'))
								.append($('<div>Not available</div>'));
						}

						jdom.appendTo($(`#${mapdiv}`));
						jdom.tooltipster({ content: tooltipcontent, arrow: false, interactive: true, offsetY: -100, position: 'right' });
					}
				});
			});
		},
		error: function(xhr, status, text) {
			//var response = $.parseJSON(xhr.responseText);
			if (text) {
				alertError(text)
			} else {
				alertError(`ERR_CONNECTION_REFUSED: This site can’t be reached.`)
			}
			loading.fadeOut()
			$('#map-image').remove()
		},
		complete: ()=>{
			// remove previous entry block
			$(`.entry-block[data-timepoint!="${mapid}-${tc}"]`).remove()
		}	
	});

	// add tag-buttom for latest used maps
	if ($(`#prev_maps > button[data-value='${mapid}']`).length == 0) {
		// remove the first map icon if it's already 5 of them
		if ($(`#prev_maps > button`).length == 5) {
			$(`#prev_maps > button`).first().remove()
		}
		let map_icon = $(`<button type="button" class="btn btn-sm btn-secondary" data-value="${mapid}">`)
		map_icon.html(mapid + " ")
		map_icon.on('click', function () {
			let id = $(this).attr('data-value')
			let [fid, t, hm] = _ctrl_params()
			drawMap("keggmap", id, display_badge, proj_dir, t, hm);
		})
		let close_icon = $('<i class="fas fa-times-circle">')
		close_icon.on("click", function () {
			$(this).parent().remove();
		})
		close_icon.appendTo(map_icon)
		map_icon.appendTo($('#prev_maps'))
	}
}

function GetURLParameter(sParam) {
	let sPageURL = window.location.search.substring(1);
	let sURLVariables = sPageURL.split('&');
	for (let i = 0; i < sURLVariables.length; i++) {
		let sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) {
			return sParameterName[1];
		}
	}
}