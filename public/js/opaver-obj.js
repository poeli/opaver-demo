class conflictHandler {
	// Filling block with the color reflecting the expression level.

	constructor(hm) {
		// hm can be one of these:
		// pos-max, neg-min, abs-max (default)
		this.hm = hm;
		this.values = [];
	}

	add_value(val) {
		this.values.push(Number(val))
	}

	resolve() {
		let display_exp_lvl;
		let absmax_exp_lvl = 0;
		let hm = this.hm;

		if (this.values.length > 1) {
			$.each(this.values, function (idx, diff) {
				// Need to maintain the max abs(diff) value, no matter which handling method is chosen.
				// When values do not have conflicts, the max abs(diff) will be used.
				if (Math.abs(diff) >= Math.abs(absmax_exp_lvl)) {
					absmax_exp_lvl = diff
				}

				if (hm == 'pos-max' && diff >= 0) {
					if (display_exp_lvl == undefined) {
						display_exp_lvl = diff
					}
					if (diff > display_exp_lvl) {
						display_exp_lvl = diff
					}
				}
				else if (hm == 'neg-min' && diff <= 0) {
					if (display_exp_lvl == undefined) {
						display_exp_lvl = diff
					}
					if (diff < display_exp_lvl) {
						display_exp_lvl = diff
					}
				}
			})

			if (display_exp_lvl == undefined) {
				display_exp_lvl = absmax_exp_lvl
			}
			return display_exp_lvl
		}
		else {
			return this.values[0]
		}
	}
}



class highlightBlock {

	constructor(body_selector) {
		this.HIGHLIGHT_CLASS = "highlight";
		this.HIGHLIGHT_ACTIVE_CLASS = "highlight-is-active";
		this.body = $(body_selector);	
		this.highlightIsActive = false;
	}

	enable(name){
		this.body.find(`.entry-block[alt*='${name}']`).addClass(this.HIGHLIGHT_CLASS);

		// if (this.highlightIsActive) return;
		// // CLICK OUTSIDE OF HIGHLIGHT ELEMENT
		// this.body.on('click', function (e) {
		// 	if (
		// 		!this.highlightIsActive ||
		// 		$(e.target).attr(this.HIGHLIGHT_CLASS)// check when HIGHLIGHTER is clicked
		// 	) return;

		// 	let $highlightedParent = $(e.target).closest('.' + this.HIGHLIGHT_CLASS);
		// 	if (!$highlightedParent.length) {
		// 		this.deactivateHighlight();
		// 	}
		// });
	}

	// HIGHLIGHT HELPERS
	activateHighlight($element) {
		this.highlightedElement = $element;
		this.highlightIsActive = true;
		this.body.addClass(this.HIGHLIGHT_ACTIVE_CLASS);
		this.highlightedElement.addClass(this.HIGHLIGHT_CLASS);
	}

	deactivateHighlight() {
		this.body.removeClass(this.HIGHLIGHT_ACTIVE_CLASS);
		this.highlightedElement.removeClass(this.HIGHLIGHT_CLASS);
		this.highlightedElement = $();
		this.highlightIsActive = false;
	}
}

class expColor {
	//class for generate color based on colorscale

	constructor(min_fold, max_fold, interpolator = "RdYlGn-r") {
		this.min_fold = Number(min_fold)
		this.max_fold = Number(max_fold)
		this.inverse = false
		let d3i = `interpolate${interpolator}`

		try {
			if (interpolator.endsWith("-r")==true){
				d3i = d3i.replace(/-r$/, '');
				this.inverse = true
			}
		} catch (error) {
			interpolator = "RdYlGn-r"
			d3i = `interpolate${interpolator}`
			this.inverse = true
			alertError(`Color interpolator "${interpolator}" is invalid.`)
		}

		this.interpolate = d3[d3i]
		let colorScale = d3.scaleSequential().domain([0,10]).interpolator(this.interpolate)

		let data = d3.range(11);
		if (this.inverse) data = data.reverse()

		let svg = d3.select("#scalebar-color");
		let rects = svg.selectAll(".rects")
					.data(data)
					.enter()
					.append("rect")
					.attr("y", 1)
					.attr("height", 30)
					.attr("x", (d, i) => i*8)
					.attr("width", 8)
					.attr("fill", (d) => colorScale(d))
	}

	get_rgb(fold) {
		if (fold > this.max_fold) {
			fold = this.max_fold
		}
		else if (fold < this.min_fold) {
			fold = this.min_fold
		}

		let t = (fold - this.min_fold) / (this.max_fold - this.min_fold)
		if (this.inverse == true) {
			t = 1 - t
		}
		return this.interpolate(t)
	}
}

let ec_desc = {
    "EC1"   :"Oxidoreductases.",
    "EC1.1" :"Acting on the CH-OH group of donors.",
    "EC1.2" :"Acting on the aldehyde or oxo group of donors.",
    "EC1.3" :"Acting on the CH-CH group of donors.",
    "EC1.4" :"Acting on the CH-NH(2) group of donors.",
    "EC1.5" :"Acting on the CH-NH group of donors.",
    "EC1.6" :"Acting on NADH or NADPH.",
    "EC1.7" :"Acting on other nitrogenous compounds as donors.",
    "EC1.8" :"Acting on a sulfur group of donors.",
    "EC1.9" :"Acting on a heme group of donors.",
    "EC1.10":"Acting on diphenols and related substances as donors.",
    "EC1.11":"Acting on a peroxide as acceptor.",
    "EC1.12":"Acting on hydrogen as donors.",
    "EC1.13":"Acting on single donors with incorporation of molecular oxygen (oxygenases). The oxygen incorporated need not be derived from O(2).",
    "EC1.14":"Acting on paired donors, with incorporation or reduction of molecular oxygen. The oxygen incorporated need not be derived from O(2).",
    "EC1.15":"Acting on superoxide as acceptor.",
    "EC1.16":"Oxidizing metal ions.",
    "EC1.17":"Acting on CH or CH(2) groups.",
    "EC1.18":"Acting on iron-sulfur proteins as donors.",
    "EC1.19":"Acting on reduced flavodoxin as donor.",
    "EC1.20":"Acting on phosphorus or arsenic in donors.",
    "EC1.21":"Catalyzing the reaction X-H + Y-H = 'X-Y'.",
    "EC1.22":"Acting on halogen in donors.",
    "EC1.23":"Reducing C-O-C group as acceptor.",
    "EC1.97":"Other oxidoreductases.",
    "EC1.98":"Enzymes using H(2) as reductant.",
    "EC1.99":"Other enzymes using O(2) as oxidant.",
    
    "EC2"   :"Transferases.",
    "EC2.1" :"Transferring one-carbon groups.",
    "EC2.2" :"Transferring aldehyde or ketonic groups.",
    "EC2.3" :"Acyltransferases.",
    "EC2.4" :"Glycosyltransferases.",
    "EC2.5" :"Transferring alkyl or aryl groups, other than methyl groups.",
    "EC2.6" :"Transferring nitrogenous groups.",
    "EC2.7" :"Transferring phosphorus-containing groups.",
    "EC2.8" :"Transferring sulfur-containing groups.",
    "EC2.9" :"Transferring selenium-containing groups.",
    "EC2.10":"Transferring molybdenum- or tungsten-containing groups.",
    
    "EC3"   :"Hydrolases.",
    "EC3.1" :"Acting on ester bonds.",
    "EC3.2" :"Glycosylases.",
    "EC3.3" :"Acting on ether bonds.",
    "EC3.4" :"Acting on peptide bonds (peptidases).",
    "EC3.5" :"Acting on carbon-nitrogen bonds, other than peptide bonds.",
    "EC3.6" :"Acting on acid anhydrides.",
    "EC3.7" :"Acting on carbon-carbon bonds.",
    "EC3.8" :"Acting on halide bonds.",
    "EC3.9" :"Acting on phosphorus-nitrogen bonds.",
    "EC3.10":"Acting on sulfur-nitrogen bonds.",
    "EC3.11":"Acting on carbon-phosphorus bonds.",
    "EC3.12":"Acting on sulfur-sulfur bonds.",
    "EC3.13":"Acting on carbon-sulfur bonds.",
    
    "EC4"   :"Lyases.",
    "EC4.1" :"Carbon-carbon lyases.",
    "EC4.2" :"Carbon-oxygen lyases.",
    "EC4.3" :"Carbon-nitrogen lyases.",
    "EC4.4" :"Carbon-sulfur lyases.",
    "EC4.5" :"Carbon-halide lyases.",
    "EC4.6" :"Phosphorus-oxygen lyases.",
    "EC4.7" :"Carbon-phosphorus lyases.",
    "EC4.99":"Other lyases.",
    
    "EC5"   :"Isomerases.",
    "EC5.1" :"Racemases and epimerases.",
    "EC5.2" :"Cis-trans-isomerases.",
    "EC5.3" :"Intramolecular oxidoreductases.",
    "EC5.4" :"Intramolecular transferases.",
    "EC5.5" :"Intramolecular lyases.",
    "EC5.6" :"Isomerases altering macromolecular conformation.",
    "EC5.99":"Other isomerases.",
    
    "EC6"   :"Ligases.",
    "EC6.1" :"Forming carbon-oxygen bonds.",
    "EC6.2" :"Forming carbon-sulfur bonds.",
    "EC6.3" :"Forming carbon-nitrogen bonds.",
    "EC6.4" :"Forming carbon-carbon bonds.",
    "EC6.5" :"Forming phosphoric ester bonds.",
    "EC6.6" :"Forming nitrogen-metal bonds.",
    
    "EC7"   :"Translocases.",
    "EC7.1" :"Catalysing the translocation of hydrons.",
    "EC7.2" :"Catalysing the translocation of inorganic cations.",
    "EC7.3" :"Catalysing the translocation of inorganic anions and their chelates.",
    "EC7.4" :"Catalysing the translocation amino acids and peptides.",
    "EC7.5" :"Catalysing the translocation carbohydrates and their derivatives.",
    "EC7.6" :"Catalysing the translocation of other compounds.",
}