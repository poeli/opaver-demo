// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'sans-serif', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

let colors = ["#a6cee3", "#1f78b4", "#b2df8a", "#33a02c", "#fb9a99", "#e31a1c", "#fdbf6f", "#ff7f00", "#cab2d6", "#6a3d9a", "#ffff99", "#b15928"]
let gene_map_analysis_data = {}

function analysis_chart(gene_map_url) {
  //load expression/annotation json file
  let res = {
    labels: [],
    con_over: [],
    con_under: [],
    enh_over: [],
    enh_under: [],
    others: []
  }

  let data_loaded = $.getJSON(gene_map_url).done(function (data) {
    gene_map_analysis_data = data

    $.each(data, function (mid, info) {
      if (!is_global_overview_map(mid) && res.labels.length <= 20) {
        res.labels.push(mid)
        res.con_over.push(info.con_over.length)
        res.enh_over.push(info.enh_over.length)
        res.enh_under.push(-info.enh_under.length)
        res.con_under.push(-info.con_under.length)
        res.others.push(info.others.length)
      }
    })

    // init the first KEGG path map (display the top one)
    let [id, t, hm] = _ctrl_params()
    id = res.labels[0]
    $('#pathway-selector').selectpicker('val', id);
    drawMap(mapdiv, id, display_badge, proj_dir, t, hm);

    let datasets = [{
      label: 'Increasing over-exp',
      backgroundColor: "#e31a1c",
      data: res.enh_over
    }, {
      label: 'Consistent over-exp',
      backgroundColor: "#fb9a99",
      data: res.con_over
    }, {
      label: 'Decreasing under-exp',
      backgroundColor: "#1f78b4",
      data: res.enh_under
    }, {
      label: 'Consistent under-exp',
      backgroundColor: "#a6cee3",
      data: res.con_under
    }, {
      label: 'Inconsisten exp',
      backgroundColor: "lightgrey",
      data: res.others
    }]

    // Bar Chart Example
    var ctx = document.getElementById("mapGeneAnalysisChart");
    var myBarChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: res.labels,
        datasets: datasets,
      },
      options: {
        maintainAspectRatio: false,
        layout: {
          padding: {
            left: 10,
            right: 25,
            top: 25,
            bottom: 0
          }
        },
        scales: {
          xAxes: [{
            gridLines: {
              display: false,
              drawBorder: false
            },
            ticks: {
              maxTicksLimit: 21
            },
            maxBarThickness: 25,
            stacked: true,
          }],
          yAxes: [{
            ticks: {
              maxTicksLimit: 7,
              padding: 5,
              // Showing abs number
              callback: function(value, index, values) {
                return Math.abs(value);
              }
            },
            gridLines: {
              color: "rgb(234, 236, 244)",
              zeroLineColor: "rgb(234, 236, 244)",
              drawBorder: false,
              borderDash: [2],
              zeroLineBorderDash: [2]
            },
            stacked: true,
            scaleLabel: {
                display: true,
                labelString: "Number of Genes",    
            },
          }],
        },
        legend: {
          display: false,
        },
        tooltips: {
          titleMarginBottom: 10,
          titleFontColor: '#6e707e',
          titleFontSize: 14,
          backgroundColor: "rgb(255,255,255)",
          bodyFontColor: "#858796",
          borderColor: '#dddfeb',
          borderWidth: 1,
          xPadding: 15,
          yPadding: 15,
          displayColors: true,
          caretPadding: 10,
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
                let label = data.datasets[tooltipItem.datasetIndex].label
                let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]
                label = `${label}: ${Math.abs(value)}`
                return label;
            },
            title: (tooltipItem, data) => {
                let mapid = tooltipItem[0].xLabel
                let text = $(`#pathway-selector > option[value="${mapid}"]`).text()
                return text.replace(/^\S+\s/, "").replace(/\s\S+$/, "");
            }
          },
        },
        onClick: function (e) {
          let clickedBar = this.getElementAtEvent(e)[0];
          if (clickedBar) {
            let [id, t, hm] = _ctrl_params()
            id = this.data.labels[clickedBar._index];
            $('#pathway-selector').selectpicker('val', id);
            drawMap(mapdiv, id, display_badge, proj_dir, t, hm);
          }
        }
      }
    });
  })
}